package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p>Profile default : True&#47;null : valid, Others : invalid</p>
     */
    public static Object scenarioStatus
     
    /**
     * <p>Profile default : authkey API default (expired)</p>
     */
    public static Object authorizationAPI
     
    /**
     * <p>Profile default : Diisi saat login berhasil</p>
     */
    public static Object treatmentPlace
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            scenarioStatus = selectedVariables['scenarioStatus']
            authorizationAPI = selectedVariables['authorizationAPI']
            treatmentPlace = selectedVariables['treatmentPlace']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
