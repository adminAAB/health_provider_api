
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String

import java.util.ArrayList

import com.kms.katalon.core.testobject.TestObject


def static "com.keyword.UI.connectDB"(
    	String IP	
     , 	String dbname	
     , 	String username	
     , 	String password	) {
    (new com.keyword.UI()).connectDB(
        	IP
         , 	dbname
         , 	username
         , 	password)
}

def static "com.keyword.UI.executeQuery"(
    	String queryString	) {
    (new com.keyword.UI()).executeQuery(
        	queryString)
}

def static "com.keyword.UI.execute"(
    	String queryString	) {
    (new com.keyword.UI()).execute(
        	queryString)
}

def static "com.keyword.UI.countdbColumn"(
    	String queryTable	) {
    (new com.keyword.UI()).countdbColumn(
        	queryTable)
}

def static "com.keyword.UI.countdbRow"(
    	String queryTable	) {
    (new com.keyword.UI()).countdbRow(
        	queryTable)
}

def static "com.keyword.UI.getValueDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	
     , 	String ColumnName	) {
    (new com.keyword.UI()).getValueDatabase(
        	IP
         , 	dbname
         , 	queryTable
         , 	ColumnName)
}

def static "com.keyword.UI.updateValueDatabase"(
    	String IP	
     , 	String dbname	
     , 	String updateQuery	) {
    (new com.keyword.UI()).updateValueDatabase(
        	IP
         , 	dbname
         , 	updateQuery)
}

def static "com.keyword.UI.getOneRowDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	) {
    (new com.keyword.UI()).getOneRowDatabase(
        	IP
         , 	dbname
         , 	queryTable)
}

def static "com.keyword.UI.getOneColumnDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	
     , 	String ColumnName	) {
    (new com.keyword.UI()).getOneColumnDatabase(
        	IP
         , 	dbname
         , 	queryTable
         , 	ColumnName)
}

def static "com.keyword.UI.getAllDataDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	) {
    (new com.keyword.UI()).getAllDataDatabase(
        	IP
         , 	dbname
         , 	queryTable)
}

def static "com.keyword.UI.getSpecificDatabase"(
    	String IP	
     , 	String dbname	
     , 	String queryTable	
     , 	int row	
     , 	int column	) {
    (new com.keyword.UI()).getSpecificDatabase(
        	IP
         , 	dbname
         , 	queryTable
         , 	row
         , 	column)
}

def static "com.keyword.UI.compareRowDBtoArray"(
    	String url	
     , 	String dbname	
     , 	String queryTable	
     , 	ArrayList listData	) {
    (new com.keyword.UI()).compareRowDBtoArray(
        	url
         , 	dbname
         , 	queryTable
         , 	listData)
}

def static "com.keyword.UI.CompareFieldtoDatabase"(
    	ArrayList ObjRep	
     , 	String url	
     , 	String dbname	
     , 	String queryTable	) {
    (new com.keyword.UI()).CompareFieldtoDatabase(
        	ObjRep
         , 	url
         , 	dbname
         , 	queryTable)
}

def static "com.keyword.UI.getFieldsValue"(
    	ArrayList ObjRep	) {
    (new com.keyword.UI()).getFieldsValue(
        	ObjRep)
}

def static "com.keyword.UI.closeDatabaseConnection"() {
    (new com.keyword.UI()).closeDatabaseConnection()
}

def static "com.keyword.UI.newTestObject"(
    	String locator	) {
    (new com.keyword.UI()).newTestObject(
        	locator)
}

def static "com.keyword.UI.AccessURL"(
    	String url	) {
    (new com.keyword.UI()).AccessURL(
        	url)
}

def static "com.keyword.UI.Sleep"(
    	int timeOut	) {
    (new com.keyword.UI()).Sleep(
        	timeOut)
}

def static "com.keyword.UI.Write"(
    	TestObject xpath	
     , 	String text	) {
    (new com.keyword.UI()).Write(
        	xpath
         , 	text)
}

def static "com.keyword.UI.WaitElement"(
    	TestObject xpath	) {
    (new com.keyword.UI()).WaitElement(
        	xpath)
}

def static "com.keyword.UI.Click"(
    	TestObject xpath	) {
    (new com.keyword.UI()).Click(
        	xpath)
}

def static "com.keyword.UI.DoubleClick"(
    	TestObject xpath	) {
    (new com.keyword.UI()).DoubleClick(
        	xpath)
}

def static "com.keyword.UI.DragAndDrop"(
    	TestObject sourceXpath	
     , 	TestObject destinationXpath	) {
    (new com.keyword.UI()).DragAndDrop(
        	sourceXpath
         , 	destinationXpath)
}

def static "com.keyword.UI.Back"() {
    (new com.keyword.UI()).Back()
}

def static "com.keyword.UI.HoverItem"(
    	TestObject xpath	) {
    (new com.keyword.UI()).HoverItem(
        	xpath)
}

def static "com.keyword.UI.DeleteWrite"(
    	TestObject xpath	
     , 	String text	) {
    (new com.keyword.UI()).DeleteWrite(
        	xpath
         , 	text)
}

def static "com.keyword.UI.SkipWrite"(
    	TestObject xpath	
     , 	String text	) {
    (new com.keyword.UI()).SkipWrite(
        	xpath
         , 	text)
}

def static "com.keyword.UI.ComboBoxSearch"(
    	TestObject Combo1	
     , 	TestObject Combo2	
     , 	String Value	) {
    (new com.keyword.UI()).ComboBoxSearch(
        	Combo1
         , 	Combo2
         , 	Value)
}

def static "com.keyword.UI.ComboBoxSearchSkip"(
    	TestObject comboOpen	
     , 	TestObject comboSearch	
     , 	String Value	) {
    (new com.keyword.UI()).ComboBoxSearchSkip(
        	comboOpen
         , 	comboSearch
         , 	Value)
}

def static "com.keyword.UI.ComboBox"(
    	TestObject Combo	
     , 	String Value	) {
    (new com.keyword.UI()).ComboBox(
        	Combo
         , 	Value)
}

def static "com.keyword.UI.MultiSelectComboBox"(
    	TestObject Combo	
     , 	String Value1	
     , 	String Value2	
     , 	String Value3	
     , 	String Value4	) {
    (new com.keyword.UI()).MultiSelectComboBox(
        	Combo
         , 	Value1
         , 	Value2
         , 	Value3
         , 	Value4)
}

def static "com.keyword.UI.CheckDisableandWrite"(
    	TestObject Xpath	
     , 	String text	) {
    (new com.keyword.UI()).CheckDisableandWrite(
        	Xpath
         , 	text)
}

def static "com.keyword.UI.RunningPhoneNumber"(
    	TestObject xpath	) {
    (new com.keyword.UI()).RunningPhoneNumber(
        	xpath)
}

def static "com.keyword.UI.UploadFile"(
    	String fileLocation	
     , 	String pictureName	) {
    (new com.keyword.UI()).UploadFile(
        	fileLocation
         , 	pictureName)
}

def static "com.keyword.UI.RunScheduler"(
    	String path	) {
    (new com.keyword.UI()).RunScheduler(
        	path)
}

def static "com.keyword.UI.WriteAllRowsXls"(
    	String path	
     , 	int row	
     , 	ArrayList value	) {
    (new com.keyword.UI()).WriteAllRowsXls(
        	path
         , 	row
         , 	value)
}

def static "com.keyword.UI.WriteSingleCellXls"(
    	String path	
     , 	int row	
     , 	int column	
     , 	Object value	) {
    (new com.keyword.UI()).WriteSingleCellXls(
        	path
         , 	row
         , 	column
         , 	value)
}

def static "com.keyword.UI.AccessURLwithPlugin"(
    	String url	
     , 	String Plugin	) {
    (new com.keyword.UI()).AccessURLwithPlugin(
        	url
         , 	Plugin)
}

def static "com.keyword.UI.readQRCode"() {
    (new com.keyword.UI()).readQRCode()
}

def static "com.kms.katalon.keyword.uploadfile.UploadFile.uploadFileUsingRobot"(
    	TestObject object	
     , 	String file	) {
    (new com.kms.katalon.keyword.uploadfile.UploadFile()).uploadFileUsingRobot(
        	object
         , 	file)
}

def static "com.kms.katalon.keyword.uploadfile.UploadFile.uploadFile"(
    	TestObject object	
     , 	String file	) {
    (new com.kms.katalon.keyword.uploadfile.UploadFile()).uploadFile(
        	object
         , 	file)
}

def static "com.keyword.REA.ComboBoxReact"(
    	TestObject Combo	
     , 	String Value	) {
    (new com.keyword.REA()).ComboBoxReact(
        	Combo
         , 	Value)
}

def static "com.keyword.REA.getArrayIcon"(
    	TestObject Object	) {
    (new com.keyword.REA()).getArrayIcon(
        	Object)
}

def static "com.keyword.REA.getArrayButton"(
    	TestObject Object	
     , 	String type	
     , 	String Class	) {
    (new com.keyword.REA()).getArrayButton(
        	Object
         , 	type
         , 	Class)
}

def static "com.keyword.REA.getColumnDataTableUI"(
    	TestObject Table	
     , 	int column	
     , 	boolean displayEmpty	) {
    (new com.keyword.REA()).getColumnDataTableUI(
        	Table
         , 	column
         , 	displayEmpty)
}

def static "com.keyword.REA.datePicker"(
    	String date	
     , 	TestObject FieldDate	) {
    (new com.keyword.REA()).datePicker(
        	date
         , 	FieldDate)
}

def static "com.keyword.REA.convertDate"(
    	String Date	
     , 	boolean time	) {
    (new com.keyword.REA()).convertDate(
        	Date
         , 	time)
}

def static "com.keyword.REA.getAllText"(
    	TestObject Cover	
     , 	String tagName	) {
    (new com.keyword.REA()).getAllText(
        	Cover
         , 	tagName)
}

def static "com.keyword.REA.getAllColumnValue"(
    	TestObject tableXpath	
     , 	String gridColumn	) {
    (new com.keyword.REA()).getAllColumnValue(
        	tableXpath
         , 	gridColumn)
}

def static "com.keyword.REA.getAllRowsValue"(
    	TestObject tableXpath	
     , 	String columnHeader	
     , 	String RowsValue	) {
    (new com.keyword.REA()).getAllRowsValue(
        	tableXpath
         , 	columnHeader
         , 	RowsValue)
}

def static "com.keyword.REA.CompareRowsValue"(
    	TestObject tableXpath	
     , 	String columnHeader	
     , 	String RowsValue	
     , 	ArrayList RowsCompare	) {
    (new com.keyword.REA()).CompareRowsValue(
        	tableXpath
         , 	columnHeader
         , 	RowsValue
         , 	RowsCompare)
}

def static "com.keyword.REA.CompareColumnsValue"(
    	TestObject tableXpath	
     , 	String gridColumn	
     , 	ArrayList CompareColumn	) {
    (new com.keyword.REA()).CompareColumnsValue(
        	tableXpath
         , 	gridColumn
         , 	CompareColumn)
}

def static "com.keyword.REA.ClickExpectedRow"(
    	TestObject tableXpath	
     , 	String gridColumn	
     , 	String columnValue	) {
    (new com.keyword.REA()).ClickExpectedRow(
        	tableXpath
         , 	gridColumn
         , 	columnValue)
}

def static "com.keyword.REA.ClickExpectedRowWithNext"(
    	TestObject tableXpath	
     , 	String gridColumn	
     , 	String columnValue	
     , 	TestObject ButtonNext	) {
    (new com.keyword.REA()).ClickExpectedRowWithNext(
        	tableXpath
         , 	gridColumn
         , 	columnValue
         , 	ButtonNext)
}

def static "com.keyword.REA.CompareColumnToDatabase"(
    	TestObject tableXpath	
     , 	String gridColumn	
     , 	String url	
     , 	String dbname	
     , 	String queryTable	
     , 	String getColumn	) {
    (new com.keyword.REA()).CompareColumnToDatabase(
        	tableXpath
         , 	gridColumn
         , 	url
         , 	dbname
         , 	queryTable
         , 	getColumn)
}

def static "com.keyword.REA.CompareRowToDatabase"(
    	TestObject tableXpath	
     , 	String columnHeader	
     , 	String RowsValue	
     , 	String url	
     , 	String dbname	
     , 	String queryTable	) {
    (new com.keyword.REA()).CompareRowToDatabase(
        	tableXpath
         , 	columnHeader
         , 	RowsValue
         , 	url
         , 	dbname
         , 	queryTable)
}

def static "com.keyword.REA.getAllDataTable"(
    	TestObject tableXpath	) {
    (new com.keyword.REA()).getAllDataTable(
        	tableXpath)
}

def static "com.keyword.REA.compareTabletoDatabase"(
    	TestObject tableXpath	
     , 	String IP	
     , 	String dbname	
     , 	String queryTable	) {
    (new com.keyword.REA()).compareTabletoDatabase(
        	tableXpath
         , 	IP
         , 	dbname
         , 	queryTable)
}
