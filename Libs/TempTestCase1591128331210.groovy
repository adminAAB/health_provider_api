import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner
import com.kms.katalon.core.mobile.contribution.MobileDriverCleaner
import com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner


DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())


RunConfiguration.setExecutionSettingFile('C:\\Users\\TMD\\AppData\\Local\\Temp\\Katalon\\Test Cases\\Scenario\\List Transaction\\Pending\\Upload Image\\Get List Document\\001 - Get List Document (Valid)\\20200603_030531\\execution.properties')

TestCaseMain.beforeStart()

        TestCaseMain.runTestCase('Test Cases/Scenario/List Transaction/Pending/Upload Image/Get List Document/001 - Get List Document (Valid)', new TestCaseBinding('Test Cases/Scenario/List Transaction/Pending/Upload Image/Get List Document/001 - Get List Document (Valid)',[:]), FailureHandling.STOP_ON_FAILURE , false)
    
