import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.REA as REA

ArrayList checkDB
String strEmail = 'shp.rumahsakitklinik@gmail.com'
String strPass = 'P@ssw0rd'
String strPageNo = '1'
String strTotalItemPage = '10'
String strDtFromTreatment = '2019/06/10'
String strDtToTreatment = '2020/06/10'
String strDtFromPayment = ''
String strDtToPayment = ''
String strMemberName = ''
String strInvoiceNo = ''
String strCompany = 'PT'
String strClaimNumber = ''
String strStatusClaim = 'paid'

GlobalVariable.scenarioStatus = 'True'

WebUI.callTestCase(findTestCase('API/Login Gen 5/Login'), [('username') : strEmail, ('password') : strPass])

/*checkDB = REA.getAllDataDatabase('172.16.94.70', 'SEA', queryGetData.replace('_FUser_', strFUser).replace('_TreatmentPlace_', GlobalVariable.treatmentPlace))

for (i = 0; i < checkDB.size; i++) {
    strMemberName = checkDB.get(i).get(0)
	println (strMemberName)
}*/

GlobalVariable.scenarioStatus = 'True'
WebUI.callTestCase(findTestCase('API/Collection and Settlement/Collection and Settlement'), [
	('pageNo') : strPageNo, ('totalItemPage') : strTotalItemPage,
	('dtFromTreatment') : strDtFromTreatment, ('dtToTreatment') : strDtToTreatment, ('dtFromPayment') : strDtFromPayment, 
	('dtToPayment') : strDtToPayment, ('memberName') : strMemberName, ('invoiceNo') : strInvoiceNo,
	('company') : strCompany, ('claimNumber') : strClaimNumber, ('statusClaim') : strStatusClaim], FailureHandling.STOP_ON_FAILURE)

