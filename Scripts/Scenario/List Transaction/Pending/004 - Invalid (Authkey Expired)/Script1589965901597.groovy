import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.REA as REA

ArrayList checkDB
String strEmail = 'shp.rumahsakitklinik@gmail.com'
String strPass = 'P@ssw0rd'
String strFUser = 'WEB'
String strPageNo = '1'
String strTotalItemPage = '10'
String strDateFrom = ''
String strDateTo = ''
String strMemberName = ''
String strTreatmentType = ''

GlobalVariable.scenarioStatus = 'False'
GlobalVariable.authorizationAPI = 'Bearer 3aWUnC79WtcI6P85w/e/IRlhyWCe4WMe9ks+kZ2UuJsLBNksaXyKoGd6TyYguEqJZVu8LF0ekEI8v8XoHjWnA8jqRj6bSBAOMS/yEOQ09Wi8DLCyM5YvlblaJIPL2RXOUpH6jcyiwgHwvw78V1McwjlW9Kx1KjKeNeFZgGo7M/c3lPTLs7X7GjYYot7EykAaP9JxkDtllsAxT0oZUXWpUUr+ezLwVfabEHbaSN5gSNohNda0c1l4xE+/14R9kKmES6o1gzQd8Me8Ah4lqpAXUd+FW4yrEOdw7CfgNqeOuoo='

checkDB = REA.getAllDataDatabase('172.16.94.70', 'SEA', queryGetData.replace('_FUser_', strFUser).replace('_TreatmentPlace_', GlobalVariable.treatmentPlace))

for (i = 0; i < checkDB.size; i++) {
    strMemberName = checkDB.get(i).get(0)
	println (strMemberName)
}

WebUI.callTestCase(findTestCase('API/List Transaction/Pending/Get List Claim Pending'), [
	('pageNo') : strPageNo, ('totalItemPage') : strTotalItemPage, ('dateFrom') : strDateFrom, ('dateTo') : strDateTo,
	('memberName') : strMemberName, ('treatmentType') : strTreatmentType, ('fUser') : strFUser], FailureHandling.STOP_ON_FAILURE)

