import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.REA as REA

ArrayList checkDB
String strEmail = 'shp.rumahsakitklinik@gmail.com'
String strPass = 'P@ssw0rd'
String strFUser = 'WEB'
String strFileID = ''
String strTab = '1'

checkDB = REA.getAllDataDatabase('172.16.94.70', 'SEA', queryGetData.replace('_TreatmentPlace_', GlobalVariable.treatmentPlace))

for (i = 0; i < checkDB.size; i++) {
    strFileID = checkDB.get(i).get(1)
	println (strFileID)
}

GlobalVariable.scenarioStatus = 'False'
WebUI.callTestCase(findTestCase('API/List Transaction/Outstanding/Upload Image/Delete Uploaded Image'), [
	('fileID') : strFileID,('fUser') : strFUser, ('tab') : strTab], FailureHandling.STOP_ON_FAILURE)

