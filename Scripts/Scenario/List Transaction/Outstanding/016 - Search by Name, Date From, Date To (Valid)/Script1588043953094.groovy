import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.REA as REA

ArrayList checkDB
String strEmail = 'shp.rumahsakitklinik@gmail.com'
String strPass = 'P@ssw0rd'
String strFUser = 'WEB'
String strPageNo = '1'
String strTotalItemPage = '10'
String strDateFrom = ''
String strDateTo = ''
String strMemberName = ''
String strTreatmentType = ''

GlobalVariable.scenarioStatus = 'True'

WebUI.callTestCase(findTestCase('API/Login Gen 5/Login'), [('username') : strEmail, ('password') : strPass])

//GET MEMBER NAME
checkDB = REA.getAllDataDatabase('172.16.94.70', 'SEA', queryGetData.replace('_TreatmentPlace_', GlobalVariable.treatmentPlace))

for (i = 0; i < checkDB.size; i++) {
    strMemberName = checkDB.get(i).get(0)
	println (strMemberName)
}

//GET DATE FROM
checkDB = REA.getAllDataDatabase('172.16.94.70', 'SEA', 'select CONVERT (varchar(10), getdate()-7, 103) AS [DD/MM/YYYY]')

for (i = 0; i < checkDB.size; i++) {
	strDateFrom = checkDB.get(i).get(0)
	println (strDateFrom)
}

//GET DATE TO
checkDB = REA.getAllDataDatabase('172.16.94.70', 'SEA', 'select CONVERT (varchar(10), getdate(), 103) AS [DD/MM/YYYY]')

for (i = 0; i < checkDB.size; i++) {
	strDateTo = checkDB.get(i).get(0)
	println (strDateTo)
}

GlobalVariable.scenarioStatus = 'True'
WebUI.callTestCase(findTestCase('API/List Transaction/Outstanding/Get List Transaction'), [
	('pageNo') : strPageNo, ('totalItemPage') : strTotalItemPage, ('dateFrom') : strDateFrom, ('dateTo') : strDateTo,
	('memberName') : strMemberName, ('treatmentType') : strTreatmentType, ('fUser') : strFUser], FailureHandling.STOP_ON_FAILURE)

