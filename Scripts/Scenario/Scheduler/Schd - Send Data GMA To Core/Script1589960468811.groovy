import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.sql.ResultSet

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI
import com.keyword.REA as REA

//CHECK DATA YANG AKAN DI PROSES (INSERT KE TABEL dbo.GMA_SchedulerTest)
ArrayList checkDB
ArrayList checkDB2
def result
ResultSet resultSet
String strConnection
String strCNO
String dbCNO
String dbDocumentNo
String strTotal
String strTotal2
String path

result = REA.updateValueDatabase('172.16.94.48', "LiTT", qDeleteDataLiTT, "sa", "Password95")


checkDB = REA.getAllDataDatabase('172.16.94.70', 'GMA', qGetData)

for (i = 0; i < checkDB.size; i++) {
	strCNO = checkDB.get(i).get(0)
	println (strCNO)
	result = REA.updateValueDatabase('172.16.94.48', "LiTT", qInsertDataLiTT.replace("_CNO_", strCNO.trim()), "sa", "Password95")
}

//JALANKAN SCHEDULER Upload Document
path = '\\\\172.16.94.33\\c$\\Installer\\SchedulerUploadDocs\\a2is.HealthProviderUploadDocument.SchedulerUploadDocs.exe'
UI.RunScheduler(path)


//CHECK DATA SETELAH SCHEDULER SELESAI
checkDB = REA.getAllDataDatabase('172.16.94.48', 'LiTT', qCheckByCNO)

for (i = 0; i < checkDB.size; i++) {
	dbCNO = checkDB.get(i).get(0)
	println (dbCNO)
	
	//check data mandatory
	checkDB2 = REA.getAllDataDatabase('172.16.94.70', 'SEA', qCheckMandatory.replace("_CNO_", dbCNO.trim()))
	dbDocumentNo = checkDB2.get(0).get(0)
	println (dbDocumentNo)
	if (dbDocumentNo == null || dbDocumentNo == '') {
		println (dbDocumentNo)
		KeywordUtil.markFailedAndStop('Check data mandatory tidak terproses dengan sempurna')
	}
	
	//CHECK ADDITIONAL INFO
	checkDB2 = REA.getAllDataDatabase('172.16.94.70', 'SEA', qCheckAdditionalInfo.replace("_DOCNO_", dbDocumentNo.trim()))
	strTotal = checkDB2.get(0).get(0).toString()
	if (strTotal != '0') {
		checkDB2 = REA.getAllDataDatabase('172.16.94.70', 'SEA', qCheckAdditionalInfo2.replace("_DOCNO_", dbDocumentNo.trim()))
		strTotal2 = checkDB2.get(0).get(0).toString()
		if (strTotal2 == '0') {
			KeywordUtil.markFailedAndStop('Check data Additional Info tidak terproses dengan sempurna')
		}
	}
	
}
/*
//JALANKAN SCHEDULER Upload Document
path = '\\\\172.16.94.33\\c$\\Installer\\SchedulerUploadDocs\\a2is.HealthProviderUploadDocument.SchedulerUploadDocs.exe'
'172.16.94.33'
UI.RunScheduler(path)*/
