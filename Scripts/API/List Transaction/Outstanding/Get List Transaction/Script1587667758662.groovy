import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.text.DateFormat

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.util.KeywordUtil
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper as JsonSlurper
import com.keyword.REA as REA

/*DATA DARI LUAR
 * GlobalVariable.authorizationAPI : kode authkey (untuk API)
 * GlobalVariable.scenarioStatus : True/null : skenario valid, Others : skenario invalid
 * GlobalVariable.treatmentPlace : kode rumahsakit (untuk check query API)
 * pageNo : halaman page (untuk API)
 * totalItemPage : total data yang tampil dalam 1 page (untuk API)
 * dateFrom : tanggal awal (untuk API)
 * dateTo : tanggal akhir (untuk API)
 * memberName : nama member bisa lengkap atau sebagian (untuk API)
 * treatmentType : kode jenis perawatan IP, MA, ODS, ODC (untuk API)
 * fUser : kode WEB, WA dll (untuk API)
*/
ArrayList checkDB
def response
def object
def jsonSlurper
def expectedData
String strMessage = ''
String strCheckDB = ''
String strQuery = ''
Integer i
Integer countDB
//DATA API
String strClaimNo
String strNamaPeserta
String strPerusahaan
String strStartDate
String strEndDate
String strTreatmentType

if (GlobalVariable.scenarioStatus == '' || GlobalVariable.scenarioStatus == null) {
	GlobalVariable.scenarioStatus = 'True'
}
println  (pageNo)
println  (totalItemPage)
println  (dateFrom)
println  (dateTo)
println(memberName)

response = WS.sendRequest(findTestObject('HealthProvider-5/List Transaction/Outstanding/Get Claim Transaction List',
	[('authorization') : GlobalVariable.authorizationAPI, ('pageNo') : pageNo, ('totalItemPage') : totalItemPage,
		('dateFrom') : dateFrom, ('dateTo') : dateTo, ('memberName') : memberName
		]))

if (memberName == "<script>alert('XSS')</script>") {
	memberName = "<script>alert(''XSS'')</script>"
}

jsonSlurper = new JsonSlurper()
object = jsonSlurper.parseText(response.getResponseBodyContent())

println (object)
println ('Status Code : '+response.getStatusCode())
if (response.getStatusCode() != 200) {
	//JIKA SCENARIO TRUE NAMUN RESPONSENYA SALAH
	if (GlobalVariable.scenarioStatus == 'True') {
		KeywordUtil.markFailedAndStop('Skenario True namun Response API : ' + response.getStatusCode())
	}
	//VALIDASI AUTHKEY NULL
	if (GlobalVariable.authorizationAPI == '' && response.getStatusCode() == 401) {
		WS.verifyElementPropertyValue(response, 'Message', 'Authorization has been denied for this request.', FailureHandling.STOP_ON_FAILURE)
	}
	//VALIDASI AUTHORIZATION INVALID
	else if (GlobalVariable.authorizationAPI == 'Bearer invalid' && response.getStatusCode() == 401) {
		strMessage = response.getResponseText()
		println (strMessage)
		if (strMessage != '"Security token incorrect"') {
			KeywordUtil.markFailedAndStop('Fail Bearer Invalid Get Registered Eligibility, Error msg  : ' + response.getStatusCode())
		}
	}
	//VALIDASI TOKEN EXPIRED
	else if (response.getStatusCode() == 440) {
		strMessage = response.getResponseText()
		if (strMessage != '"Security token expired"') {
			KeywordUtil.markFailedAndStop('Fail Token Expired Get Registered Eligibility, Error msg  : ' + response.getStatusCode())
		}
	}
	//VALIDASI RESPONSE LAINNYA
	else {
		println (response.getStatusCode())
		KeywordUtil.markFailedAndStop('Fail Check API Get List Transaction, Error code : ' + response.getStatusCode())
	}
}
//RESPONSE 200
else {
	//SCENARIO TRUE
	expectedData = object.Data.Items
	if (GlobalVariable.scenarioStatus == 'True') {
		
		println (expectedData)
		println (expectedData.size())
		if (expectedData.size() == 0) {
			KeywordUtil.markFailedAndStop('Skenario True, tetapi data yang muncul 0')
		} 
		for (i = 0; i < expectedData.size(); i++) {
			strClaimNo = object.Data.Items.ClaimNumber[i]
			strNamaPeserta = object.Data.Items.NamaPeserta[i]
			strPerusahaan = object.Data.Items.Perusahaan[i]
			strStartDate = object.Data.Items.StartTreatmentDate[i]
			strEndDate = object.Data.Items.FinishTreatmentDate[i]
			strTreatmentType = object.Data.Items.TreatmentType[i]
			println (fUser)
			println (strClaimNo)
			println (strNamaPeserta)
			println (strPerusahaan)
			println (strStartDate)
			println (strEndDate)
			println (strTreatmentType)
			println (GlobalVariable.treatmentPlace)
			
			strQuery = qCheckDB.replace('_FUser_', fUser).replace('_TreatmentPlace_', GlobalVariable.treatmentPlace).replace('_CNo_',strClaimNo).replace('_NamaPeserta_', strNamaPeserta).replace('_TreatmentType_', strTreatmentType).replace('_NamaPerusahaan_', strPerusahaan)
			println (strQuery)
			checkDB = REA.getAllDataDatabase('172.16.94.70', 'SEA',strQuery)
			strCheckDB = checkDB.get(0).get(0)
			if (strCheckDB != '1') {
				KeywordUtil.markFailedAndStop('Data yang muncul tidak sesuai : ' + strClaimNo)
			}
		}
		
		if (memberName != '') {
			strQuery = qCheckByName.replace('_TreatmentPlace_', GlobalVariable.treatmentPlace).replace('_NamaPeserta_', strNamaPeserta)
			println (strQuery)
		}
		checkDB = REA.getAllDataDatabase('172.16.94.70', 'SEA',strQuery)
		countDB = checkDB.get(0).get(0)
		i = i + 1 //ditambah 1 karena array mulai dari 0
		if (countDB != i + 1 && countDB <= 10) { 
			KeywordUtil.markFailedAndStop('Total Data yang muncul tidak sesuai DB : ' + countDB + ', API : ' + i)
		}
	} 
	//SCENARIO DATA SALAH
	else {
		if (expectedData.size() != 0) {
			KeywordUtil.markFailedAndStop('Skenario False, tetapi data yang muncul <> 0')
		}
	}
}










