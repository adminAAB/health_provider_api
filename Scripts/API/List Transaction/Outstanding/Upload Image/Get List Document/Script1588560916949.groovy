import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.text.DateFormat

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.util.KeywordUtil
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper as JsonSlurper
import com.keyword.REA as REA

/*DATA DARI LUAR
 * GlobalVariable.authorizationAPI : kode authkey (untuk API)
 * GlobalVariable.scenarioStatus : True/null : skenario valid, Others : skenario invalid
 * GlobalVariable.treatmentPlace : kode rumahsakit (untuk check query API)
 * fUser : kode WEB, WA dll (untuk API)
 * claimNo : untuk API dan check query
 * tab : untuk API 0 transact outstanding dan 1 transact pending
*/
ArrayList checkDB
def response
def object
def jsonSlurper
def expectedData
ArrayList storeProcedureA
ArrayList storeProcedureB
String strMessage = ''
String strCheckDB = ''
String strQuery = ''
Integer i
Integer countDB
//DATA API
String strImageID = ''
String strFileName = ''
String strFileExt = ''

if (GlobalVariable.scenarioStatus == '' || GlobalVariable.scenarioStatus == null) {
	GlobalVariable.scenarioStatus = 'True'
}

response = WS.sendRequest(findTestObject('HealthProvider-5/List Transaction/Outstanding/Upload Image/GetListDocument',
	[('authorization') : GlobalVariable.authorizationAPI, ('claimNo') : claimNo, ('tabs') : tab]))

if (claimNo == "<script>alert('XSS')</script>") {
	claimNo = "<script>alert(''XSS'')</script>"
}

jsonSlurper = new JsonSlurper()
object = jsonSlurper.parseText(response.getResponseBodyContent())
println (object)
println ('Status Code : '+response.getStatusCode())
if (response.getStatusCode() != 200) {
	//JIKA SCENARIO TRUE NAMUN RESPONSENYA SALAH
	if (GlobalVariable.scenarioStatus == 'True') {
		KeywordUtil.markFailedAndStop('Skenario True namun Response API : ' + response.getStatusCode())
	}
	//VALIDASI AUTHKEY NULL
	if (GlobalVariable.authorizationAPI == '' && response.getStatusCode() == 401) {
		WS.verifyElementPropertyValue(response, 'Message', 'Authorization has been denied for this request.', FailureHandling.STOP_ON_FAILURE)
	}
	//VALIDASI AUTHORIZATION INVALID
	else if (GlobalVariable.authorizationAPI == 'Bearer invalid' && response.getStatusCode() == 401) {
		strMessage = response.getResponseText()
		println (strMessage)
		if (strMessage != '"Security token incorrect"') {
			KeywordUtil.markFailedAndStop('Fail Bearer Invalid Get Registered Eligibility, Error msg  : ' + response.getStatusCode())
		}
	}
	//VALIDASI TOKEN EXPIRED
	else if (response.getStatusCode() == 440) {
		strMessage = response.getResponseText()
		if (strMessage != '"Security token expired"') {
			KeywordUtil.markFailedAndStop('Fail Token Expired Get Registered Eligibility, Error msg  : ' + response.getStatusCode())
		}
	}
	//VALIDASI RESPONSE LAINNYA
	else {
		println (response.getStatusCode())
		KeywordUtil.markFailedAndStop('Fail Check API Get List Transaction, Error code : ' + response.getStatusCode())
	}
}
//RESPONSE 200
else {
	//SCENARIO TRUE
	expectedData = object.Data
	if (GlobalVariable.scenarioStatus == 'True') {
		/*strQuery = qCheckDBFalse
		println (strQuery)
		checkDB = REA.getAllDataDatabase('172.16.94.70', 'SEA',strQuery)
		println (checkDB)
		strCheckDB = checkDB.get(0).get(0)*/
		println (expectedData)
		println (expectedData)
		println (expectedData.size())
		if (expectedData.size() == 0) {
			if (GlobalVariable.scenarioStatus == 'True') {
				KeywordUtil.markFailedAndStop('Scenario True namun list document 0 : ' + claimNo)
			}
		} else {
			if (GlobalVariable.scenarioStatus != 'True') {
				KeywordUtil.markFailedAndStop('Scenario false namun list document <> 0 : ' + claimNo)
			} /*else {
				for (int x = 0 ; x < storeProcedureB.size() ; x++) {
					if (!(storeProcedureA.contains(storeProcedureB[x]))) {
						storeProcedureA.add(storeProcedureB[x] + " (Jika dokumen ada, wajib dilampirkan)")
					}
				}
			}*/
		}
	} 
	//SCENARIO DATA SALAH
	else {
		if (object.Data != null) {
			KeywordUtil.markFailedAndStop('Skenario False, tetapi data yang muncul <> null')
		}
	}
}










