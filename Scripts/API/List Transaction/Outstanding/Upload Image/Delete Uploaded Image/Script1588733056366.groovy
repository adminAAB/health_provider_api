import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.text.DateFormat

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.util.KeywordUtil
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper as JsonSlurper
import com.keyword.REA as REA

/*DATA DARI LUAR
 * GlobalVariable.authorizationAPI : kode authkey (untuk API)
 * GlobalVariable.scenarioStatus : True/null : skenario valid, Others : skenario invalid
 * GlobalVariable.treatmentPlace : kode rumahsakit (untuk check query API)
 * fUser : kode WEB, WA dll (untuk API)
 * fileID : untuk API dan check query
 * tab : kode 0 data outstanding, 1 untuk pending
*/
ArrayList checkDB
def response
def object
def jsonSlurper
def expectedData
ArrayList storeProcedureA
ArrayList storeProcedureB
String strMessage = ''
String strCheckDB = ''
String strQuery = ''
Integer i
Integer countDB
//DATA API
String strImageID = ''
String strFileName = ''
String strFileExt = ''

if (GlobalVariable.scenarioStatus == '' || GlobalVariable.scenarioStatus == null) {
	GlobalVariable.scenarioStatus = 'True'
}

response = WS.sendRequest(findTestObject('HealthProvider-5/List Transaction/Outstanding/Upload Image/Delete Uploaded Image',
	[('authorization') : GlobalVariable.authorizationAPI, ('fileID') : fileID, ('tabs') : tab]))

if (fileID == "<script>alert('XSS')</script>") {
	fileID = "<script>alert(''XSS'')</script>"
}

jsonSlurper = new JsonSlurper()
object = jsonSlurper.parseText(response.getResponseBodyContent())
println (object)
println ('Status Code : '+response.getStatusCode())
if (response.getStatusCode() != 200) {
	//JIKA SCENARIO TRUE NAMUN RESPONSENYA SALAH
	if (GlobalVariable.scenarioStatus == 'True') {
		KeywordUtil.markFailedAndStop('Skenario True namun Response API : ' + response.getStatusCode())
	}
	//VALIDASI AUTHKEY NULL
	if (GlobalVariable.authorizationAPI == '' && response.getStatusCode() == 401) {
		WS.verifyElementPropertyValue(response, 'Message', 'Authorization has been denied for this request.', FailureHandling.STOP_ON_FAILURE)
	}
	//VALIDASI AUTHORIZATION INVALID
	else if (GlobalVariable.authorizationAPI == 'Bearer invalid' && response.getStatusCode() == 401) {
		strMessage = response.getResponseText()
		println (strMessage)
		if (strMessage != '"Security token incorrect"') {
			KeywordUtil.markFailedAndStop('Fail Bearer Invalid Get Registered Eligibility, Error msg  : ' + response.getStatusCode())
		}
	}
	//VALIDASI TOKEN EXPIRED
	else if (response.getStatusCode() == 440) {
		strMessage = response.getResponseText()
		if (strMessage != '"Security token expired"') {
			KeywordUtil.markFailedAndStop('Fail Token Expired Get Registered Eligibility, Error msg  : ' + response.getStatusCode())
		}
	}
	//VALIDASI RESPONSE LAINNYA
	else {
		println (response.getStatusCode())
		KeywordUtil.markFailedAndStop('Fail Check API Get List Transaction, Error code : ' + response.getStatusCode())
	}
}
//RESPONSE 200

else {
	strQuery = qCheckDB.replace('_ImageID_', fileID)
	println (strQuery)
	checkDB = REA.getAllDataDatabase('172.16.94.70', 'SEA',strQuery)
	strCheckDB = checkDB.get(0).get(0)
	//SCENARIO TRUE
	if (GlobalVariable.scenarioStatus == 'True') {
		if (strCheckDB != '1' ) {
			KeywordUtil.markFailedAndStop('Skenario True, data tidak masuk ke DB : ' + fileID)
		}
	} 
	//SCENARIO DATA SALAH
	else {
		println (object.Status)
		if (strCheckDB != '0' ) {
			KeywordUtil.markFailedAndStop('Skenario False, data masuk ke DB : ' + fileID)
		} else if (object.Status != false) {
			KeywordUtil.markFailedAndStop('Skenario False, Response API errordescription salah : ' + fileID)
		}
	}
}










