import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.DBData
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper as JsonSlurper

import com.kms.katalon.core.util.KeywordUtil

/*DATA DARI LUAR
 * username : untuk API body login
 * password : untuk API body login
 * GlobalVariable.scenarioStatus : True : sukses, False : failed, null : sukses
 */
if (GlobalVariable.scenarioStatus == '') {
	GlobalVariable.scenarioStatus = 'True' 
}
def response = WS.sendRequest(findTestObject('HealthProvider-5/Login/Login (username,password)', [('username') : username ,
	('password') : password]))
	println (response.getResponseBodyContent())
	def jsonSlurper = new JsonSlurper()
	def object = jsonSlurper.parseText(response.getResponseBodyContent())
	if (response.getStatusCode() != 200) {
		if (GlobalVariable.scenarioStatus == 'True') {
			KeywordUtil.markFailedAndStop('Response skenario true bukan 200!')
		}
		//AUTHORIZATION NULL
		if (GlobalVariable.authorizationAPI == '' && response.getStatusCode() == 401) {
			WS.verifyElementPropertyValue(response, 'Message', 'Authorization has been denied for this request.', FailureHandling.STOP_ON_FAILURE)
			println ('test case null OK')
		}
		//VALIDASI AUTHORIZATION INVALID
		else if (GlobalVariable.authorizationAPI == 'Bearer invalid' && response.getStatusCode() == 401) {
			strMessage = response.getResponseText()
			println (strMessage)
			if (strMessage != '"Security token incorrect"') {
				KeywordUtil.markFailedAndStop('Fail Bearer Invalid Daftar Transaksi , Error msg  : ' + response.getStatusCode())
			}
			println ('test case null OK')
		}
		//TOKEN EXPIRED
		else if (response.getStatusCode() == 440) {
			strMessage = response.getResponseText()
			println (strMessage)
			if (strMessage != '"Security token expired"') {
				KeywordUtil.markFailedAndStop('Fail Token Expired Daftar Transaksi , Error msg  : ' + response.getStatusCode())
			}
			println ('test case expired OK')
		}
		//IF ERROR CONNECTION OR OTHERS
		else {
			println (response.getStatusCode())
			KeywordUtil.markFailedAndStop('Fail Check API Daftar Transaksi, Error code : ' + response.getStatusCode())
		}
	}
	//VALID
	else if (object.status != false) {
		
		if (response.getStatusCode() == 200) {
			DBData getDataProvider = findTestData('Get Data/Login/Get Provider ID - _UserID_')
			getDataProvider.query = getDataProvider.query.replace("_UserID_", username)
			getDataProvider.fetchedData = getDataProvider.fetchData()
			GlobalVariable.treatmentPlace = getDataProvider.getValue(1, 1).toString()
			
			GlobalVariable.authorizationAPI = "Bearer "+object.token
			println (GlobalVariable.authorizationAPI)
		}
		if (response.getStatusCode() == 200 && GlobalVariable.scenarioStatus != 'True') {
			if (object.token != null) {
				KeywordUtil.markFailedAndStop('Skenario Invalid tetapi token tidak null')
			}
		}
	}
	
	else if (object.status == false) {
		if (GlobalVariable.scenarioStatus == 'True'){
			KeywordUtil.markFailedAndStop('Skenario true, response API salah')
		}
	}


