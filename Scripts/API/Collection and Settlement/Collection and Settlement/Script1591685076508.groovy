import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.text.DateFormat

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.util.KeywordUtil
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper as JsonSlurper
import com.keyword.REA as REA

/*DATA DARI LUAR
 * GlobalVariable.authorizationAPI : kode authkey (untuk API)
 * GlobalVariable.scenarioStatus : True/null : skenario valid, Others : skenario invalid
 * GlobalVariable.treatmentPlace : kode rumahsakit (untuk check query API)
 * pageNo : halaman page (untuk API)
 * totalItemPage : total data yang tampil dalam 1 page (untuk API)
 * dtFromTreatment : tanggal awal perawatan (untuk API)
 * dtToTreatment : tanggal akhir perawatan (untuk API)
 * dtFromPayment : tanggal awal pembayaran (untuk API)
 * dtToPayment : tanggal awal pembayaran (untuk API)
 * memberName : nama member bisa lengkap atau sebagian (untuk API)
 * invoiceNo : nomor invoice (untuk API)
 * company : nama perusahaan (untuk API)
 * claimNumber : no claim (untuk API)
 * statusClaim : process,paid,incomplete, ''  (untuk API)
*/
ArrayList checkDB
def response
def object
def jsonSlurper
def expectedData
String strMessage = ''
String strCheckDB = ''
String strQuery = ''
Integer i
Integer countDB
//DATA API
String strFromTreatment
String strToTreatment
String strPaymentDate
String strMemberName
String strInvoiceNo
String strCompany
String strClaimNo
String strStatusClaim
String strTotalPengajuan
String strTotalPembayaran

if (GlobalVariable.scenarioStatus == '' || GlobalVariable.scenarioStatus == null) {
	GlobalVariable.scenarioStatus = 'True'
}

println(GlobalVariable.authorizationAPI)

response = WS.sendRequest(findTestObject('HealthProvider-5/Collection and Settlement/Get List Invoice',
	[('authorization') : GlobalVariable.authorizationAPI,
		('pageNo') : pageNo, ('totalItemPage') : totalItemPage,
		('dtFromTreatment') : dtFromTreatment , ('dtToTreatment') : dtToTreatment , ('dtFromPayment') : dtFromPayment 
		, ('dtToPayment') : dtToPayment , ('memberName') : memberName , ('invoiceNo') : invoiceNo 
		, ('company') : company , ('claimNumber') : claimNumber, ('statusClaim') : statusClaim]))

println (response)

if (memberName == "<script>alert('XSS')</script>") {
	memberName = "<script>alert(''XSS'')</script>"
}

if (invoiceNo == "<script>alert('XSS')</script>") {
	invoiceNo = "<script>alert(''XSS'')</script>"
}

if (company == "<script>alert('XSS')</script>") {
	company = "<script>alert(''XSS'')</script>"
}

if (claimNumber == "<script>alert('XSS')</script>") {
	claimNumber = "<script>alert(''XSS'')</script>"
}


jsonSlurper = new JsonSlurper()
object = jsonSlurper.parseText(response.getResponseBodyContent())

println (object)
println ('Status Code : '+response.getStatusCode())
if (response.getStatusCode() != 200) {
	//JIKA SCENARIO TRUE NAMUN RESPONSENYA SALAH
	if (GlobalVariable.scenarioStatus == 'True') {
		KeywordUtil.markFailedAndStop('Skenario True namun Response API : ' + response.getStatusCode())
	}
	//VALIDASI AUTHKEY NULL
	if (GlobalVariable.authorizationAPI == '' && response.getStatusCode() == 401) {
		WS.verifyElementPropertyValue(response, 'Message', 'Authorization has been denied for this request.', FailureHandling.STOP_ON_FAILURE)
	}
	//VALIDASI AUTHORIZATION INVALID
	else if (GlobalVariable.authorizationAPI == 'Bearer invalid' && response.getStatusCode() == 401) {
		strMessage = response.getResponseText()
		println (strMessage)
		if (strMessage != '"Security token incorrect"') {
			KeywordUtil.markFailedAndStop('Fail Bearer Invalid Get Registered Eligibility, Error msg  : ' + response.getStatusCode())
		}
	}
	//VALIDASI TOKEN EXPIRED
	else if (response.getStatusCode() == 440) {
		strMessage = response.getResponseText()
		if (strMessage != '"Security token expired"') {
			KeywordUtil.markFailedAndStop('Fail Token Expired Get Registered Eligibility, Error msg  : ' + response.getStatusCode())
		}
	}
	//VALIDASI RESPONSE LAINNYA
	else {
		println (response.getStatusCode())
		KeywordUtil.markFailedAndStop('Fail Check API Get List Transaction, Error code : ' + response.getStatusCode())
	}
}
//RESPONSE 200
else {
	//SCENARIO TRUE
	expectedData = object.Data.Items
	if (GlobalVariable.scenarioStatus == 'True') {
		
		println (expectedData)
		println (expectedData.size())
		if (expectedData.size() == 0) {
			KeywordUtil.markFailedAndStop('Skenario True, tetapi data yang muncul 0')
		} 
		for (i = 0; i < expectedData.size(); i++) {
			
			strFromTreatment = object.Data.Items.StartTreatmentDate[i]
			strToTreatment = object.Data.Items.FinishTreatmentDate[i]
			strPaymentDate = object.Data.Items.PaymentDate[i]
			strMemberName = object.Data.Items.MemberName[i]
			strInvoiceNo = object.Data.Items.InvoiceNo[i]
			strCompany = object.Data.Items.Company[i]
			strClaimNo = object.Data.Items.ClaimNumber[i]
			strStatusClaim = object.Data.Items.StatusClaim[i]
			strTotalPengajuan = object.Data.Items.TotalReceipt[i]
			strTotalPembayaran = object.Data.Items.TotalPayment[i]
			strCNO = object.Data.Items.CNO[i]
			println (strFromTreatment)
			println (strToTreatment)
			println (strPaymentDate)
			println (strMemberName)
			println (strInvoiceNo)
			println (strCompany)
			println (strClaimNo)
			println (strStatusClaim)
			println (strTotalPengajuan)
			println (strTotalPembayaran)
			println (strCNO)
			println (GlobalVariable.treatmentPlace)
			
			if (strMemberName == null || strMemberName == '') {
				KeywordUtil.markFailedAndStop('Skenario true data tidak muncul! ')
			}
			/*strQuery = qCheckDB.replace('_FUser_', fUser).replace('_TreatmentPlace_', GlobalVariable.treatmentPlace).replace('_CNo_',strClaimNo).replace('_NamaPeserta_', strNamaPeserta).replace('_TreatmentType_', strTreatmentType).replace('_NamaPerusahaan_', strPerusahaan)
			println (strQuery)
			checkDB = REA.getAllDataDatabase('172.16.94.70', 'SEA',strQuery)
			strCheckDB = checkDB.get(0).get(0)
			if (strCheckDB != '1') {
				KeywordUtil.markFailedAndStop('Data yang muncul tidak sesuai : ' + strClaimNo)
			}*/
		}
		
		/*if (memberName != '' && dateFrom == '' && dateTo == '') {
			strQuery = qCheckByName.replace('_TreatmentPlace_', GlobalVariable.treatmentPlace).replace('_NamaPeserta_', strNamaPeserta).replace('_CNO_', strClaimNo)
			println (strQuery)
		} else {
		strQuery = ''
		}
		
		if (strQuery != '') {
			checkDB = REA.getAllDataDatabase('172.16.94.70', 'SEA',strQuery)
			countDB = checkDB.get(0).get(0)
			println(countDB)
			i = i + 1 //ditambah 1 karena array mulai dari 0
			if (countDB != i && countDB <= 10) { 
				KeywordUtil.markFailedAndStop('Total Data yang muncul tidak sesuai DB : ' + countDB + ', API : ' + i)
			}
		} else {
			println('fitur pengecekkan belum lengkap')
		}*/
	} 
	//SCENARIO DATA SALAH
	else {
		if (expectedData.size() != 0) {
			KeywordUtil.markFailedAndStop('Skenario False, tetapi data yang muncul <> 0')
		}
	}
}










