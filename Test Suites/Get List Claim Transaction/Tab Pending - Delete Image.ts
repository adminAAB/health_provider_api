<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Tab Pending - Delete Image</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>31659e62-8644-41c6-a301-86b6d145f19a</testSuiteGuid>
   <testCaseLink>
      <guid>243a3996-111e-4d94-99ac-e709e7182aca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/List Transaction/Pending/Upload Image/Delete Image/001 - Delete Image (Valid)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d7958af7-375e-406e-8ec6-633b211bcdb4</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>4ac98cab-ef18-4efa-93de-3d0a2df4eb5b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/List Transaction/Pending/Upload Image/Delete Image/002 - Delete Image (Invalid - FileID Others Provider)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b0a4d403-ed3f-4b52-bbf9-d96d99a019ba</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e08ce853-4773-4cf3-8df6-f49c21b68dc4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/List Transaction/Pending/Upload Image/Delete Image/003 - Delete Image (Invalid - FileID null)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1bb4a714-7546-4bcf-a6d0-2ed7957a2e1a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>63e5acf1-a14e-4dae-a671-9dfea8033691</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/List Transaction/Pending/Upload Image/Delete Image/004 - Delete Image (Invalid FileID)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4114dbed-fceb-4f64-bb15-304f995afbd5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>fb436ea6-d436-4e96-a4ea-583201544502</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/List Transaction/Pending/Upload Image/Delete Image/005 - Delete Image (Invalid Cross Site Script)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>bcc574fa-5d5f-4a29-8992-8cb5f5b6d08d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3fa3cf5b-7419-41e2-8dfc-13103c9e452c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/List Transaction/Pending/Upload Image/Delete Image/006 - Delete Image (Invalid Authkey null)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4248b11b-3088-4409-9d51-7f777bab6526</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9816a5a8-3586-46ac-aad5-6f046b836c9c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/List Transaction/Pending/Upload Image/Delete Image/007 - Delete Image (Invalid Authkey Expired)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7518aede-bcb7-4ca5-813e-4da15f5422c6</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3672902b-800a-407c-b1d1-dc25b25a4bc1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/List Transaction/Pending/Upload Image/Delete Image/008 - Delete Image (Invalid Authkey)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>cda9d601-af4a-403a-b88c-5dc87cd22e56</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ea0468e5-fef6-4d86-a999-9e3c5eabb994</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/List Transaction/Pending/Upload Image/Delete Image/009 - Delete Image (Invalid - Tabs Code Outstanding)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a60052e1-c321-407d-baf8-be76106c3534</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
