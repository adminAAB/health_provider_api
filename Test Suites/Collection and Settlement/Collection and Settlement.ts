<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Collection and Settlement</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1b686932-fc8d-4656-af78-921a0cfd9d5a</testSuiteGuid>
   <testCaseLink>
      <guid>cf5258dd-909b-4e0c-a817-7d3b993f95ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Collection and Settlement/001 - Valid Search by Status Claim Process</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0293306c-156b-4554-accf-b7c3ddabf338</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Collection and Settlement/002 - Valid Search by Status Claim Incomplete</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>83336114-5fd1-40ea-96e1-c13adaebcca3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Collection and Settlement/003 - Valid Search by Status Claim Paid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>483e7b59-5873-4405-bb78-cd82c7cb3e8d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Collection and Settlement/004 - Invalid Authkey Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af89ac6d-75d5-4e6a-8106-28e7695db01c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Collection and Settlement/005 - Invalid Authkey Expired</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7658375a-3a52-4cad-bc00-a89295595e8c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Collection and Settlement/006 - Invalid Authkey Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b93488a0-447c-442a-bc52-f34d383b44d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Collection and Settlement/007 - Valid Search by InvoiceNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eeaef8d1-4be8-4b5c-ae79-8aca94efb5d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Collection and Settlement/008 - Invalid Search by data other provider</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a8643add-8039-47e4-b788-63e820adc484</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>5fc53e30-60bc-401b-a7fa-85d3fa272680</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Collection and Settlement/009 - Invalid Search by Cross Site Script</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>21a1bc7c-7106-4645-a911-0ff72ece89aa</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
