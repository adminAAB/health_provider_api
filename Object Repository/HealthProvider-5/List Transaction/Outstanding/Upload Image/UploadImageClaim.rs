<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>UploadImageClaim</name>
   <tag></tag>
   <elementGuidId>d196a92f-56b8-4bb9-8f23-14650dfc31f8</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;multipart/form-data&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;Data&quot;,
      &quot;value&quot;: &quot;D:\\eunha.jpg&quot;,
      &quot;type&quot;: &quot;File&quot;
    },
    {
      &quot;name&quot;: &quot;FileSize&quot;,
      &quot;value&quot;: &quot;11709&quot;,
      &quot;type&quot;: &quot;Text&quot;
    },
    {
      &quot;name&quot;: &quot;FileName&quot;,
      &quot;value&quot;: &quot;eunha.jpg&quot;,
      &quot;type&quot;: &quot;Text&quot;
    },
    {
      &quot;name&quot;: &quot;FileExt&quot;,
      &quot;value&quot;: &quot;jpg&quot;,
      &quot;type&quot;: &quot;Text&quot;
    },
    {
      &quot;name&quot;: &quot;ClaimNo&quot;,
      &quot;value&quot;: &quot;2756984&quot;,
      &quot;type&quot;: &quot;Text&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>form-data</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>multipart/form-data</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer xlHIMFue+wzEHrTF/N3TA/KN7sJJ2S9F1eEqufACSdLtVPphN0mKe3UWw77E5kM0H7k9Z7INFYjdek5CiW5ViJrR7Djlp7cGFltfMujXhFSQGj+ImvbX5lzJ5q+x9Qvf4fvwvYjA1qfgA9e79K9T8wDMKTZcPN6qQmhmHkKeuzXtS5BXQwOCEcbALaoXj0KU7vSUxTHsH/gDOsPDIoqSYgXzcM19sjuyjUw9pqnT6ZP5DuIlHYfDENEReyQmQ1OCmTydl0MKxLdUJ1ew9aJS5m83qq4thiriaLmegZYdM229MV7Bv1m9GCx5xO8Xb941</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://gen5-qc.asuransiastra.com/GardaAkses/API/HealthProviderUploadDocument/UploadImageClaim</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
