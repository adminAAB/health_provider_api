<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Get List Invoice</name>
   <tag></tag>
   <elementGuidId>5c6336da-2e52-4169-99e7-f93f392ddcbb</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;PageNo&quot;,
      &quot;value&quot;: &quot;${pageNo}&quot;
    },
    {
      &quot;name&quot;: &quot;TotalItemPage&quot;,
      &quot;value&quot;: &quot;${totalItemPage}&quot;
    },
    {
      &quot;name&quot;: &quot;DtFromTreatment&quot;,
      &quot;value&quot;: &quot;${dtFromTreatment}&quot;
    },
    {
      &quot;name&quot;: &quot;DtToTreatment&quot;,
      &quot;value&quot;: &quot;${dtToTreatment}&quot;
    },
    {
      &quot;name&quot;: &quot;DtFromPayment&quot;,
      &quot;value&quot;: &quot;${dtFromPayment}&quot;
    },
    {
      &quot;name&quot;: &quot;DtToPayment&quot;,
      &quot;value&quot;: &quot;${dtToPayment}&quot;
    },
    {
      &quot;name&quot;: &quot;MemberName&quot;,
      &quot;value&quot;: &quot;${memberName}&quot;
    },
    {
      &quot;name&quot;: &quot;InvoiceNo&quot;,
      &quot;value&quot;: &quot;${invoiceNo}&quot;
    },
    {
      &quot;name&quot;: &quot;Company&quot;,
      &quot;value&quot;: &quot;${company}&quot;
    },
    {
      &quot;name&quot;: &quot;ClaimNumber&quot;,
      &quot;value&quot;: &quot;${claimNumber}&quot;
    },
    {
      &quot;name&quot;: &quot;StatusClaim&quot;,
      &quot;value&quot;: &quot;${statusClaim}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${authorization}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://gen5-qc.asuransiastra.com/GardaAkses/API/HealthCollectionSettlement/GetListInvoice</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
